## lynx setup

Instructions in this video: https://www.youtube.com/watch?v=hifs83xV2PQ

Basically - all the config here is symlinked from ~/.config.

Instead of calling lynx from /usr/bin/lynx or wherever it is installed, you
want to call the `lynx` script in this directory, so you have to make sure that
is in your path. That is a wrapper script that sets the necessary environment
variables for you.
