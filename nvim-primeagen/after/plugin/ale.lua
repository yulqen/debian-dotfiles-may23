-- Set the configuration options for the plugin
local linters = {
    python = {'ruff', 'flake8', 'pyright'},
    ocaml = {'merlin'},
    cpp = {'clang'},
    yaml = {'yamllint'},
    c = {'clang'},
    go = {'gopls', 'golint', 'gofmt'},
}

local fixers = {
    python = {'ruff', 'isort', 'yapf', 'black', 'autoimport'},
    go = {'gofmt', 'goimports'},
    rust = {'rustfmt'},
}

local pyright_config = {
    useLibraryCodeForTypes = 1,
    disableLanguageServices = 1,
    autoImportCompletions = 1,
}

vim.api.nvim_set_var('ale_linters', linters)
vim.api.nvim_set_var('ale_python_pyright_config', pyright_config)
vim.api.nvim_set_var('ale_fix_on_save', 1)
vim.api.nvim_set_var('ale_warn_about_trailing_whitespace', 1)
vim.api.nvim_set_var('ale_disable_lsp', 1)
vim.api.nvim_set_var('ale_use_neovim_diagnostics_api', 1)
vim.api.nvim_set_var('ale_set_quickfix', 1)
vim.api.nvim_set_var('ale_echo_cursor', 1)
vim.api.nvim_set_var('ale_echo_msg_error_str', 'Error')
vim.api.nvim_set_var('ale_echo_msg_form', '%linter% - %code: %%s')
vim.api.nvim_set_var('ale_loclist_msg_format', '%linter% - %code: %%s')
vim.api.nvim_set_var('ale_echo_msg_warning_s', 'Warning')
vim.api.nvim_set_var('ale_fixers', fixers)
vim.api.nvim_set_var('ale_python_mypy_ignore_invalid_syntax', 1)
vim.api.nvim_set_var('ale_python_mypy_executable', 'mypy')
vim.api.nvim_set_var('ale_python_mypy_options', '--config-file mypy.ini')
vim.api.nvim_set_var('g:ale_sign_error', '>>')
vim.api.nvim_set_var('ale_fix_on_save', 1)
vim.api.nvim_set_var('ale_linters_explicit', 0)
