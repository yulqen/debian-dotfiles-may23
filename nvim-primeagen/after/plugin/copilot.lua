vim.g.copilot_no_tab_map = true
vim.api.nvim_set_keymap("i", "<C-J>", 'copilot#Accept("<CR>")', { silent = true, expr = true })
-- not working vim.g.copilot_assume_mapped = true

vim.g.copilot_filetypes = {
    ["*"] = false,
    ["markdown"] = false,
    ["javascript"] = false,
    ["typescript"] = false,
    ["lua"] = true,
    ["rust"] = true,
    ["c"] = true,
    ["c#"] = true,
    ["c++"] = true,
    ["go"] = true,
    ["python"] = true,
}
