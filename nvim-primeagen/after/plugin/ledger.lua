-- easily make a transaction starred with <leader>-
vim.keymap.set("n", "<leader>-", "<cmd>call ledger#transaction_state_set(line('.'), '*')<CR>")

vim.api.nvim_set_var("ledger_detailed_first", 0)
