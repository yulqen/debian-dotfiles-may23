local has_words_before = function()
  unpack = unpack or table.unpack
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end


local ls = require("luasnip")

ls.filetype_extend("htmldjango", {"html"})

require("luasnip.loaders.from_lua").lazy_load({ paths = "~/.config/nvim/snippets" })
require("luasnip.loaders.from_vscode").lazy_load()

ls.config.set_config({
    history = true,
    updateevents = "TextChanged, TextChangedI", --update changes as you type
    enable_autosnippets = true,
    ext_opts = {
        [require("luasnip.util.types").choiceNode] = {
            active = {
                virt_text = { { ">" } },
            },
        },
    },
})
