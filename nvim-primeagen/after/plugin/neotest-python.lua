require("neotest").setup({
    quickfix = {
        open = true,
        enabled = true,
    },
    status = {
        enabled = true,
        signs = true, -- Sign after function signature
        virtual_text = false
    },
    icons = {
        child_indent = "│",
        child_prefix = "├",
        collapsed = "─",
        expanded = "╮",
        failed = "✘",
        final_child_indent = " ",
        final_child_prefix = "╰",
        non_collapsible = "─",
        passed = "✓",
        running = "",
        running_animated = { "/", "|", "\\", "-", "/", "|", "\\", "-" },
        skipped = "↓",
        unknown = ""
    },
    floating = {
        border = "rounded",
        max_height = 0.9,
        max_width = 0.9,
        options = {}
    },
    summary = {
        open = "botright vsplit | vertical resize 60"
    },
    highlights = {
        adapter_name = "NeotestAdapterName",
        border = "NeotestBorder",
        dir = "NeotestDir",
        expand_marker = "NeotestExpandMarker",
        failed = "NeotestFailed",
        file = "NeotestFile",
        focused = "NeotestFocused",
        indent = "NeotestIndent",
        marked = "NeotestMarked",
        namespace = "NeotestNamespace",
        passed = "NeotestPassed",
        running = "NeotestRunning",
        select_win = "NeotestWinSelect",
        skipped = "NeotestSkipped",
        target = "NeotestTarget",
        test = "NeotestTest",
        unknown = "NeotestUnknown"
    },
    adapters = {
        require("neotest-python")({
            -- Extra arguments for nvim-dap configuration
            -- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
            dap = { justMyCode = false },
            -- Command line arguments for runner
            -- Can also be a function to return dynamic values
            args = {"--log-level", "DEBUG"},
            -- Runner to use. Will use pytest if available by default.
            -- Can be a function to return dynamic value.
            runner = "pytest",
            -- Custom python path for the runner.
            -- Can be a string or a list of strings.
            -- Can also be a function to return dynamic value.
            -- If not provided, the path will be inferred by checking for 
            -- virtual envs in the local directory and for Pipenev/Poetry configs
            python = ".venv/bin/python",
            -- Returns if a given file path is a test file.
            -- NB: This function is called a lot so don't perform any heavy tasks within it.
        })
    },
})

require("neodev").setup({
  library = { plugins = { "neotest" }, types = true },
  ...
})

vim.keymap.set("n", "tn", function()
    require("neotest").run.run()
end)

vim.keymap.set("n", "tf", function()
    require("neotest").run.run(vim.fn.expand("%"))
end)

vim.keymap.set("n", "td", function()
    require("neotest").run.run({strategy = "dap"})
end)

vim.keymap.set("n", "tr", function()
    local neotest = require("neotest")
    neotest.run.run()
    neotest.summary.open()
end,
{ noremap = true, silent = true, nowait = true })

vim.keymap.set("n", "ta", function()
    local neotest = require("neotest")
    neotest.run.attach()
end)

vim.keymap.set("n", "to", function()
    local neotest = require("neotest")
    neotest.output.open({ last_run = true, enter = true })
end,
{ noremap = true, silent = true, nowait = true })

vim.keymap.set("n", "tt", function()
    local neotest = require("neotest")
    neotest.summary.toggle()
end,
{ noremap = true, silent = true, nowait = true })
