require('todotxt-nvim').setup({
    todo_file = "/home/lemon/Documents/Notes/todo/todo.txt",
    capture = {
        alternative_priority = {
            A = "today",
            B = "tomorrow",
            C = "this week",
            D = "next week",
            E = "next month",
        },
    },
    highlights = {
        priorities = {
            A = {
                fg = "yellow",
                bg = "NONE",
                style = "bold",
            },
            B = {
                fg = "green",
                bg = "NONE",
                style = "bold",
            },
            C = {
                fg = "cyan",
                bg = "NONE",
                style = "bold",
            },
            D = {
                fg = "magenta",
                bg = "NONE",
                style = "bold",
            },
        },
    },
    sidebar = {
        width = 70,
        position = "right", -- default: "right"
    },
})

-- Keyboard shortcuts for todotxt-nvim.
vim.keymap.set("n", "<localleader>tp", "<cmd>ToDoTxtTasksToggle<CR>")
vim.keymap.set("n", "<localleader>ta", "<cmd>ToDoTxtCapture<CR>")
