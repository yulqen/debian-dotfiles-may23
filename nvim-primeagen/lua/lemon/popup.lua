-- Lemon_win_id = nil
-- Lemon_bufh = nil

-- local function close_menu(force_save)
--     force_save = force_save or false

--     vim.api.nvim_win_close(Lemon_win_id, true)

--     Lemon_win_id = nil
--     Lemon_bufh = nil
-- end

-- local function create_window()
--     local popup = require("plenary.popup")
--     local height = 10
--     local width = 60
--     local borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" }
--     local bufnr = vim.api.nvim_create_buf(false, true)
--     local Lemon_id, win = popup.create(bufnr, {
--         title = "Mesmeric!",
--         line = math.floor(((vim.o.lines - height) / 2) -1),
--         col = math.floor((vim.o.columns - width) / 2),
--         minwidth = width,
--         mineheight = height,
--         borderchars = borderchars,
--     })
--     return {
--         bufnr = bufnr,
--         win_id = Lemon_win_id
--     }
-- end

-- create_window()

-- Create a new buffer and set its lines to the contents of myfile.txt
local buf = vim.api.nvim_create_buf(false, true)
local file = io.open('/home/lemon/Documents/Notes/Archive/Dr Haugh.md', 'r')
local height = 10
local width = 60
local lines = {}
for line in file:lines() do
    table.insert(lines, line)
end
file:close()
vim.api.nvim_buf_set_lines(buf, 0, -1, true, lines)

-- Create a new window and set its options to create a floating buffer
local win = vim.api.nvim_open_win(buf, true, {
    relative='editor',
    width=80,
    title = "test title",
    title_pos = "center",
    height=10,
    row=10,
    col=10,
    border={"╭", "─", "╮", "│", "╯", "─",  "╰", "│" },
})

