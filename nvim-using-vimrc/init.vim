set nocompatible
syntax on			"turn syntax highlighting on
filetype plugin indent on	"load plugin and indent files associated a detected filetype

" macro to go to end of sentence and add a line break (for
" one-line-per-sentence vimming...)
let @s = ")i\<BS>\<CR>\<Esc>"

" Open vimrc
nnoremap <leader>ev <C-w>s<C-w>j<C-w>L:e $MYVIMRC<cr>

" leader
let maplocalleader = "\\"
let mapleader = ","

" Function to allow adding a line of text to taskwarrior
function! TaskWarriorAddCurrentLine()
    let current_line = getline('.')
    silent execute ":!task add " . shellescape(current_line)
    redraw!
    echo "Task added: " . current_line
    delete
endfunction

nnoremap <leader>q :call TaskWarriorAddCurrentLine()<CR>


" redirect any output to a scratch buffer
" from https://gist.github.com/romainl/eae0a260ab9c135390c30cd370c20cd7
function! Redir(cmd, rng, start, end)
	for win in range(1, winnr('$'))
		if getwinvar(win, 'scratch')
			execute win . 'windo close'
		endif
	endfor
	if a:cmd =~ '^!'
		let cmd = a:cmd =~' %'
			\ ? matchstr(substitute(a:cmd, ' %', ' ' . shellescape(escape(expand('%:p'), '\')), ''), '^!\zs.*')
			\ : matchstr(a:cmd, '^!\zs.*')
		if a:rng == 0
			let output = systemlist(cmd)
		else
			let joined_lines = join(getline(a:start, a:end), '\n')
			let cleaned_lines = substitute(shellescape(joined_lines), "'\\\\''", "\\\\'", 'g')
			let output = systemlist(cmd . " <<< $" . cleaned_lines)
		endif
	else
		redir => output
		execute a:cmd
		redir END
		let output = split(output, "\n")
	endif
	vnew
	let w:scratch = 1
	setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile
	call setline(1, output)
endfunction

" This command definition includes -bar, so that it is possible to "chain" Vim commands.
" Side effect: double quotes can't be used in external commands
command! -nargs=1 -complete=command -bar -range Redir silent call Redir(<q-args>, <range>, <line1>, <line2>)

" This command definition doesn't include -bar, so that it is possible to use double quotes in external commands.
" Side effect: Vim commands can't be "chained".
command! -nargs=1 -complete=command -range Redir silent call Redir(<q-args>, <range>, <line1>, <line2>)

" big from https://gist.github.com/romainl/047aca21e338df7ccf771f96858edb86
nnoremap ;s :g//#<Left><Left>
function! CCR()
    let cmdline = getcmdline()
    if cmdline =~ '\v\C^(ls|files|buffers)'
        " like :ls but prompts for a buffer command
        return "\<CR>:b"
    elseif cmdline =~ '\v\C/(#|nu|num|numb|numbe|number)$'
        " like :g//# but prompts for a command
        return "\<CR>:"
    elseif cmdline =~ '\v\C^(dli|il)'
        " like :dlist or :ilist but prompts for a count for :djump or :ijump
        return "\<CR>:" . cmdline[0] . "j  " . split(cmdline, " ")[1] . "\<S-Left>\<Left>"
    elseif cmdline =~ '\v\C^(cli|lli)'
        " like :clist or :llist but prompts for an error/location number
        return "\<CR>:sil " . repeat(cmdline[0], 2) . "\<Space>"
    elseif cmdline =~ '\C^old'
        " like :oldfiles but prompts for an old file to edit
        set nomore
        return "\<CR>:sil se more|e #<"
    elseif cmdline =~ '\C^changes'
        " like :changes but prompts for a change to jump to
        set nomore
        return "\<CR>:sil se more|norm! g;\<S-Left>"
    elseif cmdline =~ '\C^ju'
        " like :jumps but prompts for a position to jump to
        set nomore
        return "\<CR>:sil se more|norm! \<C-o>\<S-Left>"
    elseif cmdline =~ '\C^marks'
        " like :marks but prompts for a mark to jump to
        return "\<CR>:norm! `"
    elseif cmdline =~ '\C^undol'
        " like :undolist but prompts for a change to undo
        return "\<CR>:u "
    else
        return "\<CR>"
    endif
endfunction
cnoremap <expr> <CR> CCR()



" encryptio) method when using :X
"set cm=blowfish2

" Underline the current line, based on its length.
noremap <silent> <leader>ul mmyypVr-<Esc>`m
"
" Show the 'list' characters.
noremap <silent> <leader>ls :set list!<CR>

" Remove double- or single-quotes, or graves wrapped around a string.
noremap <silent> <leader>rdq mmF"xf"x`m
noremap <silent> <leader>rsq mmF'xf'x`m
noremap <silent> <leader>rg mmF`xf`x`m
 
" Jump up or down by 10 lines.
noremap <silent> J 10j
noremap <silent> K 10k

" Place timestamps, be it date (YYYY-MM-DD) or time (HH:MM:SS).
if (exists("*strftime"))
	noremap <silent> <leader>date "=strftime("%F")<CR>p9h
	noremap <silent> <leader>time "=strftime("%X")<CR>p7h
endif
"
" Execute the current line with BASH.
noremap <silent> <leader>rl :.w !bash<CR>

" Run the current file with PERL, Python, BASH, or a Bourne Shell derivative.
noremap <silent> <leader>rpl :!clear; perl %<CR>
noremap <silent> <leader>rpy :!clear; python %<CR>
noremap <silent> <leader>rb :!clear; bash %<CR>
noremap <silent> <leader>rs :!clear; sh %<CR>

" essential plugins
call plug#begin()
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'lambdalisue/fern.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/fzf.vim'
Plug 'github/copilot.vim'
Plug 'sheerun/vim-polyglot'
Plug 'preservim/vim-markdown'
Plug 'mhinz/vim-signify'
Plug 'vim-test/vim-test'
Plug 'ledger/vim-ledger'
Plug 'jlanzarotta/bufexplorer'
Plug 'tpope/vim-fugitive'
Plug 'fatih/vim-go', {'do': ':GoUpdateBinaries' }
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-commentary'
"Plug 'ycm-core/YouCompleteMe'
"Plug 'davidhalter/jedi-vim'
Plug 'SirVer/UltiSnips'
Plug 'honza/vim-snippets'
Plug 'dense-analysis/ale'
call plug#end()

" jedi-vim
let g:jedi#goto_command = "<leader>d"
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_stubs_command = "<leader>s"
let g:jedi#goto_definitions_command = ""
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#rename_command = "<leader>r"
let g:jedi#rename_command_keep_name = "<leader>R"

" " YCM
" let g:ycm_enable_inlay_hints = 1
" nnoremap <silent> <localleader>h <Plug>(YCMToggleInlayHints)
" nnoremap gd :YcmCompleter GoToDefinition<CR>
" nnoremap <leader>gr :YcmCompleter GoToReferences<CR>
" nnoremap K :YcmCompleter GetDoc<CR>
" " this will disable tab, allowing it to be used for ultisnips
" let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
" let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
" let g:ycm_python_interpreter_path = '.venv/bin/python3'
" let g:ycm_auto_trigger = 1
" let g:ycm_enable_inlay_hints = 0
" let g:ycm_python_sys_path = []
" let g:ycm_show_diagnostics_ui = 0
" let g:ycm_extra_conf_vim_data = [
"   \  'g:ycm_python_interpreter_path',
"   \  'g:ycm_python_sys_path'
"   \]
" let g:ycm_global_ycm_extra_conf = '~/.global_extra_conf.py'
" nnoremap <leader>jd :YcmCompleter GoTo<CR>
" imap <silent> <C-l> <Plug>(YCMToggleSignatureHelp)'.


" snippets
" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
set completefunc=UltiSnips#Complete

" vim-test
nmap <silent> tn :TestNearest<CR>
nmap <silent> tf :TestFile<CR>
nmap <silent> ts :TestSuite<CR>
nmap <silent> tl :TestLast<CR>
nmap <silent> <leader>tv :TestVisit<CR>
let test#strategy = "basic"
let test#python#pytest#options = '-q -s'
let test#python#runner = 'pytest'
let test#vimterminal#term_position = "belowright"

" to run tests automatically
"augroup test
"  autocmd!
"  autocmd BufWrite * if test#exists() |
"    \   TestFile |
"    \ endif
"augroup END

set shell=/bin/sh
set hi=500
set scrolloff=0
" set novisualbell
"set relativenumber	"show line numbers
set wildignore=**/__pycache*/**
set wildmenu	"enable a menu that shows tab completion options in the status bar
set wildchar=<TAB>
set showmatch	"highlights matching brackets on cursor hover
set ruler	"show cursor position in status bar
set showcmd	"shows the normal mode command before it gets executed
set encoding=utf-8
set fileformats=unix,dos,mac
set nohlsearch	"highlights searches
set incsearch	"incremental search (searches character by character)
set ignorecase	"ignores the case of a search
set smartindent
set autoindent
set smartcase	"only ignores case if there are no capital letters in search (only works after ignorecase has been set)
set tabstop=4		"the amount of spaces that vim will equate to a tab character
set softtabstop=4	"like tabstop, but for editing operations (insert mode)
set splitbelow " split horiz below
set noswapfile
set splitright " split new to the right when doing vertical
set shiftwidth=4	"used for autoindent and << and >> operators in normal mode
set autoindent		"copies indent from current line to the next line
set expandtab		"tabs will expand to whitespace characters
set ttimeoutlen=20	"timeout for a key code mapping
set timeoutlen=1000	"time(ms) to wait for key mappings
set hidden
set t_Co=256
"set t_Co=0 - switches off all colours
set path+=**
set relativenumber
"set spell
set number
set more
set signcolumn=yes
set colorcolumn=0
set equalalways
set showmode
set nobackup
set listchars=tab:»→,trail:␣
set shortmess+=c
set complete+=kspell
set completeopt=menuone,longest,preview
set omnifunc=syntaxcomplete#Complete
if executable('ag')
    set grepprg=ag\ --vimgrep\ --nogroup\ --nocolor
endif


" From Practical Vim p101 - Easy expansion of Active File Directory
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" i don't bother with folding, but it's here if i need it.
if has('folding')
	set foldmethod=marker
	set foldmarker=#\ {{{,#\ }}}
	set viewoptions=folds,options,cursor,unix,slash
endif

" from https://www.reddit.com/r/vim/comments/f5gi2g/vim_notetaking_automatic_link_creation_between/
" copies a link from file from previous buffer for pasting into
" next buffer with "ap
function! s:copy_filename_as_mdlink()
	    let fname=expand("%")
		let @a="[" . fname . "](./" . fname. ")"
endfunction
autocmd BufLeave * call s:copy_filename_as_mdlink()



" vim-test
" use vim-dispatch to run tests in the quickfix window
" from Modern Vim Ch.4
" the mappings below are from vim-test
" https://github.com/vim-test/vim-test
" dispatch opens send test output to quickfix window
let test#strategy = "basic"
let test#python#runner = "pytest"
let test#vim#term_position = "topleft 10"
let test#python#pytest#options = '--tb=short'
let test#go#runner = 'gotest'
"let test#go#gotest#options = '-v'
" To run mypy using vim-dispath -with Dispatch
autocmd FileType python let b:dispatch = 'mypy --ignore-missing-imports'

" Set wrap option for Markdown files
autocmd FileType markdown setlocal linebreak

" test
nmap <silent> tn :TestNearest<CR>
nmap <silent> ts :TestSuite<CR>
nmap <silent> tf :TestFile<CR>
nmap <silent> <S-F10> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>

" disable folding by default with vim-markdown
let g:vim_markdown_folding_disabled = 1
"
" ALE ale config
let g:ale_enabled = 0
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
let g:ale_lint_on_text_changed = 0
let g:ale_hover_cursor = 0
let g:ale_virtualtext_cursor = 'disabled'
let g:ale_sign_column_always = 1
let g:ale_open_list = 0
let g:ale_set_highlights = 1
let g:ale_set_signs = 1
let g:ale_set_loclist = 1
let g:ale_set_quickfix = 0
let g:ale_echo_cursor = 1
let g:ale_echo_msg_error_str = 'Error'
let g:ale_echo_msg_format = '%linter% - %code: %%s'
let g:ale_loclist_msg_format = '%linter% - %code: %%s'
let g:ale_echo_msg_warning_str = 'Warning'
let g:ale_linters = {'python': ['flake8', 'mypy'],
\                    'ocaml': ['merlin'],
\                    'cpp': ['clang'],
\                    'yaml': ['yamllint'],
\                    'c': ['clang'],
\                    'go': ['gopls', 'golint', 'gofmt'],
 \}
let g:ale_fixers = {
\           'python': ['isort', 'yapf', 'black'],
\           'go': ['gofmt'],
\           'rust': ['rustfmt']
\           }
let g:ale_python_mypy_ignore_invalid_syntax = 1
let g:ale_python_mypy_executable = 'mypy'
let g:ale_python_mypy_options = '--config-file mypy.ini'
" let g:ale_sign_error = '>>'
let g:ale_fix_on_save = 1
let g:ale_linters_explicit = 0

" Python - keybindings
nnoremap <leader>Ts :setlocal makeprg=pytest\ -q\ %<cr>
nnoremap <leader>Tf :setlocal makeprg=flake8\ %<cr>
nnoremap <leader>Tb :setlocal makeprg=black\ %<cr>
nnoremap <leader>Tm :setlocal makeprg=mypy\ --ignore-missing-imports\ %<cr>
" then run the one set with this...
nnoremap <leader>Tt :make!<cr>

" clear search highlights
nnoremap <leader><space> :noh<cr>:call clearmatches()<cr>

" previews in netrw
let g:netrw_preview = 1

" journal stuff
autocmd BufNewFile,BufReadPost *.md map <leader>jj <Esc>:r! date +" - \%H:\%M: "<ENTER>kJA<Esc>$<space><Esc> 
autocmd BufNewFile,BufReadPost *.md map <leader>jd <Esc>:r! date +"(\%Y-\%m-\%dT\%H:\%M)"<ENTER>i<backspace><space><Esc>

syntax on			"turn syntax highlighting on
filetype plugin indent on	"load plugin and indent files associated a detected filetype
runtime macros/matchit.vim	"allows jumping between brackets with % in normal mode

" Go stuff
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_operators = 1
let g:go_doc_keywordprg_enabled = 1
let g:go_list_height = 0
let g:go_highlight_operators = 1
let g:go_highlight_function_parameters = 1
let g:go_doc_max_height = 50
let g:go_doc_popup_window = 1
let g:go_list_height = 10


" vim-go debugger window settings
let g:go_debug_windows = {
        \ 'vars':       'leftabove 40vnew',
        \ 'stack':      'leftabove 20new',
        \ 'goroutines': 'botright 10new',
        \ 'out':        'botright 5new',
\ }

" vim-go and gopls
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
let g:go_play_browser_command = 'firefox %URL% &'
let g:go_test_show_name = 1
let g:go_auto_type_info = 1
let test#go#runner = 'gotest'
let g:go_term_mode = "split"
let g:go_term_height = 50
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1

"autocmd FileType go nmap <leader>r :w<CR>:split <bar> terminal go run %<CR>
autocmd FileType go nmap <leader>R :GoRun<CR>
" highlights the variable in the file for you..
"let g:go_auto_sameids = 1
" auto import...
let g:go_fmt_command = "goimports"
" automatic type info on cursor
let g:go_auto_type_info = 2
let g:go_snippet_engine = "ultisnips"
au Filetype go nmap <leader>ga <Plug>(go-alternate-edit)
au Filetype go nmap <leader>gah <Plug>(go-alternate-split)
au Filetype go nmap <leader>gav <Plug>(go-alternate-vertical)
au Filetype go nmap <leader>gaf <Plug>(go-fmt)
au Filetype go nmap <leader>gal <Plug>(go-lint)
au Filetype go nmap <leader>gbx <Plug>(go-deps)
au FileType go nmap <F8> :GoTestFunc -short<cr>
au FileType go nmap <F10> :GoTest -short<cr>
au FileType go nmap <F9> :DlvToggleBreakpoint<CR>
au FileType go nmap <S-F9> :DlvTest<CR>
au FileType go nmap <F11> :DlvDebug<CR>
au FileType go nmap <S-F5> :GoRename<CR>

" FZF
" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" - FZF Popup window (center of the screen)
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', '#f8f8f2'],
  \ 'bg':      ['bg', '#282a36'],
  \ 'hl':      ['fg', '#bd93f9'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', '#ffb86c'],
  \ 'prompt':  ['fg', '#50fa7b'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', '#ffb86c'],
  \ 'header':  ['fg', '#6272a4'] }
"  More fzf settings
" (https://github.com/zenbro/dotfiles/blob/master/.nvimrc#L151-L187)
 let g:fzf_nvim_statusline = 0 " disable statusline overwriting

 " copy the path of the current file into the system clipboard
"nnoremap <leader>y :let @+=expand('%:p')<CR>:call system("true")<CR>:let @"=@+<CR>:echo "File path copied to clipboard: " . @+<CR>
nnoremap <leader>y :let @*=expand('%:p')<CR>:call system("true")<CR>:let @+=@*<CR>:echo "File path copied to clipboard: " . @+<CR>

" Alternatively, format is as Markdown link
nnoremap <leader>Y :let @+=printf("[%s](%s)", expand('%:t'), "file://" . expand('%:p'))<CR>:call system("true")<CR>:let @"=@+<CR>:echo "Markdown link copied to clipboard: " . @+<CR>

" command history is :History:
  nnoremap <C-s> :GFiles!<CR>
  nnoremap <leader><C-p> :<C-u>FZF!<CR>
  nnoremap <leader>t :Files<CR>
  nnoremap <leader>o :Tags<CR>
  nnoremap <leader>h :History<CR>
  nnoremap <silent> <leader>0 :Files<CR>
  nnoremap <silent> <leader>; :BLines<CR>
  nnoremap <silent> <leader>l :Lines<CR>
  nnoremap <silent> <leader>o :BTags<CR>
  nnoremap <silent> <leader>b :Buffers<CR>
  nnoremap <silent> <leader>? :History:<CR>
  nnoremap <silent> <leader>/ :execute 'Ag ' . input('Ag/')<CR>
  nnoremap <silent> <leader>ft :Filetypes<CR>
  nnoremap <silent> <leader>CC :Commands<CR>
  imap <C-x><C-f> <plug>(fzf-complete-file-ag)
  imap <C-x><C-l> <plug>(fzf-complete-line)

" remap :W to :w - :W was previous Windows in fzf
command! W w

" ghetto note system
" Go to index of notes
nnoremap <leader>ni :e $NOTES_DIR/index.md<CR>:cd $NOTES_DIR<CR>
" Depends on grepprg being set to rg
command! -nargs=1 Ngrep grep "<args>" -g "*.md" $NOTES_DIR
nnoremap <leader>nn :Ngrep 

" open quicklist vertical
command! Vlist botright vertical copen | vertical resize 50
nnoremap <leader>v :Vlist<CR>

" Search with ripgrep
command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --column --line-number --no-heading --color=always --ignore-case '.shellescape(<q-args>), 1,
      \   <bang>0 ? fzf#vim#with_preview('up:60%')
      \           : fzf#vim#with_preview('right:50%:hidden', '?'),
      \   <bang>0)

nnoremap <C-p>a :Rg 

"" Quick Editing vimrc
nnoremap <leader>ev <C-w>s<C-w>j<C-w>L:e $MYVIMRC<cr>

" syntax enable
let g:solarized_termcolors=256
let g:gruvbox_termcolors=256
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_contrast_light='hard'
let g:gruvbox_hls_cursor='orange'
let g:gruvbox_sign_column='bg0'
let g:gruvbox_number_column='bg0'
let g:gruvbox_invert_signs='0'
let g:gruvbox_improved_strings='0'
set background=dark
" colorscheme gruber-darker
colorscheme default

" Make sign column black
highlight SignColumn guibg=blue ctermbg=black

function! Grep(...)
        return system(join([&grepprg] + [expandcmd(join(a:000, ' '))], ' '))
endfunction
command! -nargs=+ -complete=file_in_path -bar Grep  cgetexpr Grep(<f-args>)
command! -nargs=+ -complete=file_in_path -bar LGrep lgetexpr Grep(<f-args>)
augroup quickfix
        autocmd!
            autocmd QuickFixCmdPost cgetexpr cwindow
                autocmd QuickFixCmdPost lgetexpr lwindow
augroup END

" manual highlights
" highlight Visual ctermfg=black ctermbg=LightMagenta
" highlight SignColumn ctermbg=black ctermfg=white
" highlight Comment ctermfg=DarkGray
" highlight DiffAdd term=bold ctermfg=yellow ctermbg=black
" highlight DiffDelete term=bold ctermfg=red ctermbg=black
" highlight DiffChange term=bold ctermfg=black ctermbg=DarkGreen

augroup general
    autocmd!
    "keep equal proportions when windows resized
    autocmd VimResized * wincmd =
    "save cursor position in a file
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"")
                \ <= line("$") | exe "normal! g'\"" | endif
augroup END

augroup languages
    autocmd!
    autocmd BufNewFile,BufRead *.bash set syntax=sh
    autocmd FileType python xnoremap <leader>r <esc>:'<,'>:w !python3<CR>
    autocmd FileType go set noexpandtab
    autocmd FileType html :syntax sync fromstart
    autocmd FileType html,javascript,css,json,yaml,sh
                \ setlocal ts=2 sts=2 sw=2 expandtab
augroup END

" Switching off colours
"syntax off
" set nohlsearch
" set background=dark
"set cursorline
"set cursorcolumn
"set visualbell
" highlight clear
" highlight Visual term=reverse cterm=reverse gui=reverse
