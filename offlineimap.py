#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import check_output

def get_pass(account):
    print(account)
    return check_output("pass " + account, shell=True).splitlines()[0]
